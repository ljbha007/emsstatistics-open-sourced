/**
 * Created by "Carl Lee"<ljbha007@gmail.com> on 2015/6/12.
 */
(function(){
    var DataTableToolbar = window.DataTableToolbar = Backbone.View.extend({
        el: '.toolbar',
        template: _.template('\
                <div class="btns">\
                    <a id="delete_selected_btn" class="btn btn-primary" href="#" role="button">删除选中条目</a>\
                    <a id="clear_btn" class="btn btn-danger" href="#" role="button">清空所有条目</a>\
                </div>\
                <form class="form bg-info" action="<%= collection.url %>">\
                    <% _.each(fields, function(field){ %>\
                        <input id="<%= field.id||"" %>" class="<%= field.className||"" %>" value="<%= field.defaultVal||"" %>" type="<%= field.type||"text" %>" step="<%= field.step||"" %>" placeholder="<%= field.placeholder||"" %>"/>\
                    <% }); %>\
                    <button class="btn btn-primary pull-right">添加新条目</button>\
                </form>'),
        events:{
            'click #delete_selected_btn' : 'deleteSelected',
            'click #clear_btn' : 'clear',
            'submit .form' : 'submit'
        },
        initialize: function(){
        },
        render: function(){
            this.$el.html(this.template(this.options));
            return this;
        },
        submit: function(e){
            e.preventDefault();
            e.stopPropagation();
            var children = this.$el.children('form').children().toArray();
            var obj = {};
            var model = new this.model();
            children.forEach(function(child){
                if(child.id != undefined && child.id.length != 0) {
                    var value = child.value == '' ? null: child.value;
                    obj[child.id] = value;
                    model.set(child.id, value);
                }
            });
            debugger;
            model.save();
            this.collection.fetch();
        },
        deleteSelected: function(e){
            e.preventDefault();
            e.stopPropagation();
            var confirmed = confirm("确定要删除选中的数据吗？");
            if(confirmed){
                if(this.options.grid.getSelectedModels != undefined){
                    var collection = this.collection;
                    var grid = this.options.grid;
                    var models = grid.getSelectedModels();
                    models.forEach(function(model){
                        // work around for model.url bug with PageableCollection
                        model.url = model.url + "/" + encodeURIComponent(model.id)
                        model.destroy({
                            success: function(model, response){
                                collection.fetch();
                                grid.clearSelectedModels();
                            },
                            error:function(model, response){
                                collection.fetch();
                                grid.clearSelectedModels();
                            }
                        });
                    });
                }
            }
        },
        clear: function(e){
            e.preventDefault();
            e.stopPropagation();
            var confirmed = confirm("确定要删除所有数据吗？");
            if(confirmed){
                var collection = this.collection;
                var url = this.collection.url;
                $.ajax({
                    url: url,
                    method: 'DELETE'})
                .done(function(result){
                    console.log(JSON.stringify(result));
                    collection.fetch();
                })
                .fail(function(result){
                    console.log(JSON.stringify(result));
                    var msg = "删除失败";
                    if(result['responseJSON'] != undefined){
                        msg += ":\n" + result['responseJSON']['msg'];
                    }
                    alert(msg);
                });
            }
        }
    });
})();