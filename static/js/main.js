/**
 * Created by "Carl Lee"<ljbha007@gmail.com> on 2015/6/8.
 */
(function(){
    var height = window.innerHeight;
    $('body').height(height);
    Dropzone.options.fileUpload = {
        dictDefaultMessage: "点击或拖拽Excel文件到此处以导入数据",
        dictFallbackText: "点击此处以导入数据",
        init: function(){
            this.on('addedfile', function(file){
                if(!file.type.match(/image.*/)){
                    this.emit('thumbnail', file, '/static/img/ic_excel.png');
                }
            });
        }
    }
})();