/**
 * Created by "Carl Lee"<ljbha007@gmail.com> on 2015/6/7.
 */
(function () {
    var SpecialRate = Backbone.Model.extend({
        url: '/ajax/special_rates',
        initialize: function(){
            this.listenTo(this, 'backgrid:edited', this.onChange);
        },
        onChange: function(model, column, options){
            console.log(model, column, options);
            if(model.hasChanged()){
                model.save();
            }
        }
    });
    var SpecialRates = Backbone.PageableCollection.extend({
        model: SpecialRate,
        url: '/ajax/special_rates',
        mode: 'server'
    });
    var collection = new SpecialRates();
    var columns = [{
          name: "",
          cell: "select-row",
          headerCell: "select-all"
    },{
          name: "id",
          label: "ID",
          cell: "string"
    },{
          name: "client_name",
          label: "客户名称",
          cell: "string"
    },{
          name: "proxy_rate",
          label: "货代折扣",
          cell: "string"
    },{
          name: "client_rate",
          label: "客户折扣",
          cell: "string"
    },{
          name: "start_date",
          label: "开始日期",
          cell: "string"
    },{
          name: "end_date",
          label: "结束日期",
          cell: "string"
    }];
    var grid = new Backgrid.Grid({
        columns: columns,
        collection: collection
    });
    var paginator = new Backgrid.Extension.Paginator({
        collection: collection
    });
    var toolbar = new DataTableToolbar({
        collection: collection,
        model: SpecialRate,
        grid: grid,
        fields: [{
          id: 'client_name',
          placeholder: '寄件人名称'
        },{
          id: 'proxy_rate',
          placeholder: '货代折扣',
          type: 'number',
          defaultVal: '0.8',
          step: 'any'
        },{
          id: 'client_rate',
          placeholder: '客户折扣',
          type: 'number',
          defaultVal: '0.8',
          step: 'any'
        },{
          id: 'start_date',
          placeholder: '开始日期',
          className: 'dates'
        },{
          id: 'end_date',
          placeholder: '结束日期',
          className: 'dates'
        }]});
    toolbar.render();
    $("#data_table").append(grid.render().el);
    $("#pagination").append(paginator.render().el);
    $(".dates").datepicker({
      format: 'yyyymmdd',
      todayBtn: "linked",
      language: 'zh-CN'
    });

    collection.fetch();

    // debugger;
}());