/**
 * Created by "Carl Lee"<ljbha007@gmail.com> on 2015/6/7.
 */
(function () {
    var Record = Backbone.Model.extend({
        initialize: function(){
            this.listenTo(this, 'backgrid:edited', this.onChange);
        },
        onChange: function(model, column, options){
            console.log(model, column, options);
        }
    });
    var Records = Backbone.PageableCollection.extend({
        model: Record,
        url: '/ajax/records',
        mode: 'server'
    });
    var collection = new Records();
    var columns = [{
          name: "country",
          label: "寄达国",
          editable: false,
          cell: "string"
    },{
          name: "serial_no",
          label: "单号",
          editable: false,
          cell: "string"
    },{
          name: "items_count",
          label: "件数",
          editable: false,
          cell: "string"
    },{
          name: "weight",
          label: "重量",
          editable: false,
          cell: "string"
    },{
          name: "recipient_name",
          label: "收件人",
          editable: false,
          cell: "string"
    },{
          name: "recipient_address",
          label: "收件人地址",
          editable: false,
          cell: "string"
    },{
          name: "recipient_cellphone",
          label: "收件人手机",
          editable: false,
          cell: "string"
    },{
          name: "recipient_tel",
          label: "收件人电话",
          editable: false,
          cell: "string"
    },{
          name: "sender_name",
          label: "寄件人",
          editable: false,
          cell: "string"
    },{
          name: "sender_address",
          label: "寄件人地址",
          editable: false,
          cell: "string"
    },{
          name: "sender_cellphone",
          label: "寄件人手机",
          editable: false,
          cell: "string"
    },{
          name: "sender_tel",
          label: "寄件人电话",
          editable: false,
          cell: "string"
    },{
          name: "batch_no",
          label: "批次",
          editable: false,
          cell: "string"
    }];
    var grid = new Backgrid.Grid({
        columns: columns,
        collection: collection
    });
    var paginator = new Backgrid.Extension.Paginator({
        collection: collection
    });
    $("#data_table").append(grid.render().el);
    $("#pagination").append(paginator.render().el);
    $("#clear_records").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var confirmed = confirm("确定要删除选中的数据吗？");
        if(confirmed){
            var url = collection.url;
            $.ajax({
                url: url,
                method: 'DELETE'})
            .done(function(result){
                console.log(JSON.stringify(result));
                collection.fetch();
            })
            .fail(function(result){
                console.log(JSON.stringify(result));
                var msg = "删除失败";
                if(result['responseJSON'] != undefined){
                    msg += ":\n" + result['responseJSON']['msg'];
                }
                alert(msg);
            });
        }
    });

    collection.fetch();

    // debugger;
}());