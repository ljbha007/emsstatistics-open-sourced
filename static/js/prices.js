/**
 * Created by "Carl Lee"<ljbha007@gmail.com> on 2015/6/7.
 */
(function () {
    var Price = Backbone.Model.extend({
        initialize: function(){
            this.listenTo(this, 'backgrid:edited', this.onChange);
        },
        onChange: function(model, column, options){
            console.log(model, column, options);
        }
    });
    var Prices = Backbone.PageableCollection.extend({
        model: Price,
        url: '/ajax/prices',
        mode: 'server'
    });
    var collection = new Prices();
    var columns = [{
          name: "country_code",
          label: "国家代码",
          editable: false,
          cell: "string"
    },{
          name: "country_name",
          label: "国家名字",
          editable: false,
          cell: "string"
    },{
          name: "price_zone",
          label: "计费区",
          cell: "string"
    },{
          name: "price",
          label: "标准价格(元/kg)",
          cell: "number"
    },{
          name: "online_price",
          label: "线上价格(元/kg)",
          cell: "number"
    },];
    var grid = new Backgrid.Grid({
        columns: columns,
        collection: collection
    });
    var paginator = new Backgrid.Extension.Paginator({
        collection: collection
    });
    $("#data_table").append(grid.render().el);
    $("#pagination").append(paginator.render().el);

    collection.fetch();

    // debugger;
}());