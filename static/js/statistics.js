/**
 * Created by "Carl Lee"<ljbha007@gmail.com> on 2015/6/7.
 */
(function () {
    var Statistic = Backbone.Model.extend({
        initialize: function(){
            this.listenTo(this, 'backgrid:edited', this.onChange);
        },
        onChange: function(model, column, options){
            console.log(model, column, options);
        },
        getters:{
            //proxy_price_sum: function(){
            //    var proxy_rate = this.get('proxy_rate');
            //    var normal_price_sum = this.get('normal_price_sum');
            //    if(proxy_rate == undefined || normal_price_sum == undefined){
            //        return undefined;
            //    }else{
            //        return proxy_rate * normal_price_sum;
            //    }
            //},
            //client_price_sum: function(){
            //    var client_rate = this.get('client_rate');
            //    var normal_price_sum = this.get('normal_price_sum');
            //    if(client_rate == undefined || normal_price_sum == undefined){
            //        return undefined;
            //    }else{
            //        return client_rate * normal_price_sum;
            //    }
            //},
            //price_difference: function(){
            //    var online_price_sum = this.get('online_price_sum');
            //    var proxy_price_sum = this.get('proxy_price_sum');
            //    if(online_price_sum == undefined || proxy_price_sum == undefined){
            //        return undefined;
            //    }else{
            //        return online_price_sum - proxy_price_sum;
            //    }
            //},
            //client_refund: function(){
            //    var online_price_sum = this.get('online_price_sum');
            //    var client_price_sum = this.get('client_price_sum');
            //    if(online_price_sum == undefined || client_price_sum == undefined){
            //        return undefined;
            //    }else{
            //        return online_price_sum - client_price_sum;
            //    }
            //},
            //profit: function(){
            //    var price_difference = this.get('price_difference');
            //    var client_refund = this.get('client_refund');
            //    if(price_difference == undefined || client_refund == undefined){
            //        return undefined;
            //    }else{
            //        return price_difference - client_refund;
            //    }
            //}
        }
    });
    var Statistics = Backbone.PageableCollection.extend({
        model: Statistic,
        url: '/ajax/statistics',
        mode: 'client'
    });
    var collection = new Statistics();
    var columns = [{
          name: "client_name",
          label: "客户名",
          editable: false,
          cell: "string"
    },{
          name: "manager",
          label: "客户经理",
          editable: false,
          cell: "string"
    },{
          name: "normal_price_sum",
          label: "标准价格",
          editable: false,
          cell: "number"
    },{
          name: "online_price_sum",
          label: "线上价格",
          editable: false,
          cell: "number"
    },{
          name: "proxy_price_sum",
          label: "货代价格",
          editable: false,
          cell: "number"
    },{
          name: "client_price_sum",
          label: "客户折后",
          editable: false,
          cell: "number"
    },{
          name: "price_difference",
          label: "折价差额",
          editable: false,
          cell: "number"
    },{
          name: "client_refund",
          label: "客户返额",
          editable: false,
          cell: "number"
    },{
          name: "profit",
          label: "利润",
          editable: false,
          cell: "number"
    }];
    var grid = new Backgrid.Grid({
        columns: columns,
        collection: collection
    });
    var paginator = new Backgrid.Extension.Paginator({
        collection: collection
    });
    var filter = {};
    $("#data_table").append(grid.render().el);
    $("#pagination").append(paginator.render().el);
    $(".dates").datepicker({
      format: 'yyyymmdd',
      todayBtn: "linked",
      language: 'zh-CN'
    });
    $(".toolbar form").submit(function(e){
        e.preventDefault();
        e.stopPropagation();
        var client_name = $("#client_name").val();
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        filter['client_name'] = client_name == "" ? undefined: client_name;
        filter['start_date'] = start_date == "" ? undefined: start_date;
        filter['end_date'] = end_date == "" ? undefined: end_date;
        console.log(filter);
        collection.fetch({
            data: filter
        });
    });
    $("#export_excel").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var url = "/export?";
        var params = [];
        for(var attr in filter){
            if(filter[attr] != undefined){
                params.push(attr + "=" + encodeURIComponent(filter[attr]));
            }
        }
        url += params.join("&");
        window.open(url);
    });

    collection.fetch();

    // debugger;
}());