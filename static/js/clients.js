/**
 * Created by "Carl Lee"<ljbha007@gmail.com> on 2015/6/7.
 */
(function () {
    var Client = Backbone.Model.extend({
        url: function(){
          var suffix = this.id == undefined ? "": "/" + encodeURIComponent(this.id)
          return '/ajax/clients' + suffix;
        },
        initialize: function(){
            this.listenTo(this, 'backgrid:edited', this.onChange);
        },
        onChange: function(model, column, options){
            if(model.hasChanged()){
                model.save();
            }
        }
    });
    window.Client = Client;
    var Clients = Backbone.PageableCollection.extend({
        model: Client,
        state: {
            pageSize: 20
        },
        url: '/ajax/clients',
        mode: 'server',
        comparator: function(c1, c2){
          return c1.get('id') >= c2.get('id')? -1: 1;
        }
    });
    var columns = [{
          name: "",
          cell: "select-row",
          headerCell: "select-all"
    },{
          name: "id",
          label: "ID",
          cell: "string"
    },{
          name: "sender_name",
          label: "寄件人名称",
          cell: "string"
    },{
          name: "sender_cellphone",
          label: "寄件人电话",
          cell: "string"
    },{
          name: "manager",
          label: "客户经理",
          cell: "string"
    },{
          name: "client_name",
          label: "客户名称",
          cell: "string"
    },{
          name: "client_rate",
          label: "客户折扣",
          cell: "number"
    },{
          name: "proxy_rate",
          label: "货代折扣",
          cell: "number"
    },{
          name: "description",
          label: "备注",
          cell: "string"
    },{
          name: "account",
          label: "账户",
          cell: "string"
    }];
    var collection = new Clients();
    var grid = new Backgrid.Grid({
        columns: columns,
        collection: collection
    });
    var paginator = new Backgrid.Extension.Paginator({
        collection: collection
    });
    var toolbar = new DataTableToolbar({
        collection: collection,
        model: Client,
        grid: grid,
        fields: [{
          id: 'sender_name',
          placeholder: '寄件人名称'
        },{
          id: 'sender_cellphone',
          placeholder: '寄件人电话(不可重复)'
        },{
          id: 'manager',
          placeholder: '客户经理'
        },{
          id: 'client_name',
          placeholder: '客户名称'
        },{
          id: 'client_rate',
          placeholder: '客户折扣',
          type: 'number',
          defaultVal: '0.8',
          step: 'any'
        },{
          id: 'proxy_rate',
          placeholder: '货代折扣',
          type: 'number',
          defaultVal: '0.8',
          step: 'any'
        },{
          id: 'description',
          placeholder: '备注'
        },{
          id: 'account_name',
          placeholder: '账户'
        }]
    });
    toolbar.render();
    $("#data_table").append(grid.render().el);
    $("#pagination").append(paginator.render().el);
    // $("#clear_clients_btn").click(function(e){
    //     e.preventDefault();
    //     e.stopPropagation();
    //     var confirmed = confirm("确定要删除所有客户数据吗？");
    //     if(confirmed){
    //         $.ajax('/ajax/clear_clients')
    //             .done(function(result){
    //                 console.log(JSON.stringify(result));
    //                 collection.fetch();
    //             })
    //             .fail(function(result){
    //                 console.log(JSON.stringify(result));
    //                 var msg = "删除失败";
    //                 if(result['responseJSON'] != undefined){
    //                     msg += ":\n" + result['responseJSON']['msg'];
    //                 }
    //                 alert(msg);
    //             });
    //     }
    // });

    collection.fetch();
    window.grid = grid;
    //debugger;
}());