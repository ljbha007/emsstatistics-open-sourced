# -*- mode: python -*-
a = Analysis(['EMSStatistics.py'],
             pathex=['D:\\Projects\\python\\EMSStatistics'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='EMSStatistics.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True , icon='favicon.ico')
extras = Tree('static', prefix='static')
extras += Tree('templates', prefix='templates')
extras += Tree('tmp', prefix='tmp')
extras += [('data.db', 'db.db', 'DATA')]
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               extras,
               strip=None,
               upx=True,
               name='EMSStatistics')
