#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, Response, send_from_directory
from openpyxl import load_workbook, Workbook
from openpyxl.styles import Border, Side, PatternFill, Color, fills
from openpyxl.utils import get_column_letter
import os
import json
import data
import webbrowser

app = Flask(__name__)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static/img'), 'favicon.png')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/records')
def records():
    return render_template('records.html')


@app.route('/prices')
def prices():
    return render_template('prices.html')


@app.route('/clients')
def clients():
    return render_template('clients.html')


@app.route('/special_rates')
def special_rates():
    return render_template('special_rates.html')


@app.route('/statistics')
def statistics():
    return render_template('statistics.html')


@app.route('/export')
def export():
    client_name = request.args.get('client_name')
    start_date = request.args.get('start_date')
    end_date = request.args.get('end_date')
    wheres = []
    if client_name is not None:
        wheres.append("clients.client_name like \"%%%s%%\"" % client_name)
    if start_date is not None:
        wheres.append("date(records.record_date) >= date(%s)" % start_date)
    if end_date is not None:
        wheres.append("date(records.record_date) <= date(%s)" % end_date)
    where = " AND ".join(wheres)
    if len(wheres) == 0:
        where = "1 = 1"
    result = data.calculate_statistics_with_details(where)
    sums = result[0]
    details = result[1]
    wb = Workbook()
    sheet = wb.active
    sheet.title = u"账务统计"
    titles = [u"日期", u"国家", u"批号", u"件数", u"重量(g)", u"收件人姓名", u"收件人地址", u"收件人手机",
              u"收件人电话", u"寄件人姓名", u"寄件人地址", u"寄件人手机", u"寄件人电话", u"运单号",
              u"客户名", u"客户经理", u"标准价格", u"线上价格", u"货代价格", u"客户折后", u"折价差额",
              u"客户返额", u"利润"]
    title_keys = ["record_date", "country", "batch_no", "items_count", "weight", "recipient_name", "recipient_address",
                  "recipient_cellphone", "recipient_tel", "sender_name", "sender_address", "sender_cellphone",
                  "sender_tel", "serial_no",
                  "client_name", "manager", "normal_price", "online_price", "proxy_price",
                  "client_price", "price_difference", "client_refund", "profit"]
    sum_keys = ["record_date", "country", "batch_no", "items_count", "weight", "recipient_name", "recipient_address",
                "recipient_cellphone", "recipient_tel", "sender_name", "sender_address", "sender_cellphone",
                "sender_tel", "serial_no",
                "client_name", "manager", "normal_price_sum", "online_price_sum", "proxy_price_sum",
                "client_price_sum", "price_difference", "client_refund", "profit"]
    border = Border(left=Side(border_style="thin",
                    color='FF000000'),
                    right=Side(border_style="thin",
                    color='FF000000'),
                    top=Side(border_style="thin",
                    color='FF000000'),
                    bottom=Side(border_style="thin",
                    color='FF000000'))
    fill = PatternFill(fill_type=fills.FILL_SOLID,
                       fgColor=Color(indexed=22L),
                       bgColor=Color(indexed=64L))
    # Set titles
    for i in range(1, len(titles) + 1):
        c = sheet.cell(column=i, row=1, value=titles[i-1])
        c.border = border
        c.fill = fill
    # Set details data
    for i in range(2, len(details) + 2):
        row = details[i-2]
        for j in range(0, len(title_keys)):
            title_key = title_keys[j]
            value = row[title_key]
            c = sheet.cell(column=j+1, row=i, value=value)
            c.border = border
    last_row = len(details) + 2
    # Set summary data
    for i in range(1, len(titles) + 1):
        c = sheet.cell(column=i, row=last_row, value=titles[i-1])
        c.border = border
        c.fill = fill
        row_cnt = last_row + 1
        for sum_row in sums:
            sum_key = sum_keys[i-1]
            if sum_key in sum_row:
                value = sum_row[sum_key]
                c = sheet.cell(column=i, row=row_cnt, value=value)
                c.border = border
            row_cnt += 1
    # Set column widths
    for i in range(1, len(titles) + 1):
        letter = get_column_letter(i)
        sheet.column_dimensions[letter].width = 20
    file_path = os.path.join(os.path.dirname(__file__), './tmp/export.xlsx')
    if os.path.exists(file_path):
        os.remove(file_path)
    wb.save(file_path)
    return send_from_directory(os.path.join(app.root_path, 'tmp'), 'export.xlsx')


@app.route('/ajax/records', methods=['GET'])
def ajax_records():
    try:
        page = int(request.args.get('page', 0)) - 1
        page_size = int(request.args.get('per_page', 20))
    except ValueError:
        page = 0
        page_size = 20
    result = data.query_table_with_pagination('records', page, page_size)
    return Response(json.dumps(result), mimetype='application/json')


@app.route('/ajax/records', methods=['DELETE'])
def ajax_clear_records():
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        data.clear_records()
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500

    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/prices')
def ajax_prices():
    try:
        page = int(request.args.get('page', 0)) - 1
        page_size = int(request.args.get('per_page', 20))
    except ValueError:
        page = 0
        page_size = 20
    result = data.query_table_with_pagination('prices', page, page_size)
    return Response(json.dumps(result), mimetype='application/json')


@app.route('/ajax/clients', methods=['GET'])
def ajax_client():
    try:
        page = int(request.args.get('page', 0)) - 1
        page_size = int(request.args.get('per_page', 20))
    except ValueError:
        page = 0
        page_size = 20
    result = data.query_table_with_pagination('clients', page, page_size, 'id', 'DESC')
    return Response(json.dumps(result), mimetype='application/json')


@app.route('/ajax/clients', methods=['POST'])
def ajax_add_client():
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        json_data = request.get_json(force=True)
        data.insert_dict_into_table('clients', [json_data])
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500
    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/clients', methods=['PUT'])
def ajax_update_client():
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        json_data = request.get_json(force=True)
        id_val = json_data.pop('id')
        data.update_dict_in_table('clients', json_data, id_val)
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500
    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/clients', methods=['DELETE'])
def ajax_clear_clients():
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        data.clear_clients()
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500

    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/clients/<int:client_id>', methods=['DELETE'])
def ajax_delete_client(client_id):
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        data.delete_client(client_id)
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500

    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/special_rates', methods=['GET'])
def ajax_special_rates():
    try:
        page = int(request.args.get('page', 0)) - 1
        page_size = int(request.args.get('per_page', 20))
    except ValueError:
        page = 0
        page_size = 20
    result = data.query_table_with_pagination('special_rate', page, page_size)
    return Response(json.dumps(result), mimetype='application/json')


@app.route('/ajax/special_rates', methods=['POST'])
def ajax_add_special_rate():
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        json_data = request.get_json(force=True)
        data.insert_dict_into_table('special_rate', [json_data])
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500
    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/special_rates', methods=['PUT'])
def ajax_update_special_rates():
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        json_data = request.get_json(force=True)
        id_val = json_data.pop('id')
        data.update_dict_in_table('special_rate', json_data, id_val)
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500
    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/special_rates', methods=['DELETE'])
def ajax_clear_special_rates():
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        data.clear_special_rates()
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500

    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/special_rates/<int:rate_id>', methods=['DELETE'])
def ajax_delete_special_rate(rate_id):
    result = {
        'code': 0,
        'msg': 'success',
        'status': 200
    }
    try:
        data.delete_special_rate(rate_id)
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500

    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/statistics')
def ajax_statistics():
    client_name = request.args.get('client_name')
    start_date = request.args.get('start_date')
    end_date = request.args.get('end_date')
    wheres = []
    if client_name is not None:
        wheres.append("clients.client_name like \"%%%s%%\"" % client_name)
    if start_date is not None:
        wheres.append("date(records.record_date) >= date(%s)" % start_date)
    if end_date is not None:
        wheres.append("date(records.record_date) <= date(%s)" % end_date)
    where = " AND ".join(wheres)
    if len(wheres) == 0:
        where = "1 = 1"
    result = data.calculate_statistics(where)
    return Response(json.dumps(result), mimetype='application/json')


@app.route('/ajax/load_records', methods=['POST'])
def load_records():
    result = {
        'code': 0,
        'msg': 'ok',
        'status': 200
    }
    try:
        f = request.files['file']
        file_path = os.path.join(os.path.dirname(__file__), './tmp/' + f.filename)
        f.save(file_path)
        book = load_workbook(file_path)
        names = book.get_sheet_names()
        if len(names) > 0:
            sheet = book.get_sheet_by_name(names[0])
            rows = []
            row_cnt = 0
            for row in sheet.rows:
                if row_cnt >= 2:
                    row_array = []
                    col_cnt = 0
                    for cell in row:
                        if col_cnt != 5 and col_cnt != 10:
                            row_array.append(cell.value)
                        if col_cnt == 14 and cell.value is not None:
                            date = cell.value[1:9]
                            row_array.append(date)
                        col_cnt += 1
                    if row_array[1] is not None:
                        rows.append(row_array)
                row_cnt += 1
            data.insert_records(rows)
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500
    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/load_prices', methods=['POST'])
def load_prices():
    result = {
        'code': 0,
        'msg': 'ok',
        'status': 200
    }
    try:
        f = request.files['file']
        file_path = os.path.join(os.path.dirname(__file__), './tmp/' + f.filename)
        f.save(file_path)
        book = load_workbook(file_path, data_only=True)
        names = book.get_sheet_names()
        if len(names) > 0:
            sheet = book.get_sheet_by_name(names[0])
            rows = []
            row_cnt = 0
            for row in sheet.rows:
                if row_cnt >= 1:
                    row_array = []
                    for cell in row:
                        row_array.append(cell.value)
                    rows.append(row_array)
                row_cnt += 1
            data.clear_prices()
            data.insert_prices(rows)
        else:
            result['code'] = 1
            result['msg'] = "no sheet available"
    except Exception as e:
        result['code'] = 1
        result['msg'] = e.message
        result['error'] = e.message
        result['status'] = 500
    return Response(json.dumps(result), mimetype='application/json'), result['status']


@app.route('/ajax/load_clients', methods=['POST'])
def load_clients():
    result = {
        'code': 0,
        'msg': 'ok',
        'status': 200
    }
    try:
        f = request.files['file']
        file_path = os.path.join(os.path.dirname(__file__), './tmp/' + f.filename)
        f.save(file_path)
        book = load_workbook(file_path, data_only=True)
        names = book.get_sheet_names()
        if len(names) > 0:
            sheet = book.get_sheet_by_name(names[0])
            rows = []
            row_cnt = 0
            for row in sheet.rows:
                if row_cnt >= 1:
                    row_array = []
                    for cell in row:
                        row_array.append(cell.value)
                    rows.append(row_array)
                row_cnt += 1
            data.insert_clients(rows)
        else:
            result['code'] = 1
            result['msg'] = result['error'] = "no sheet available"
            result['status'] = 500

    except Exception as e:
        result['code'] = 1
        result['msg'] = result['error'] = e.message
        result['status'] = 500
    return Response(json.dumps(result), mimetype='application/json'), result['status']

if __name__ == '__main__':
    debug = False
    tmp_path = os.path.join(os.path.dirname(__file__), './tmp')
    if not os.path.exists(tmp_path):
        os.makedirs(tmp_path)
    if not debug:
        webbrowser.open_new("http://localhost:5000/records")
    app.run(debug=True, host='0.0.0.0', port=5000)
