__author__ = '"Carl Lee"<ljbha007@gmail.com>'
import sqlite3
import os
from math import ceil

file_path = os.path.join(os.path.dirname(__file__), 'data.db')
db_con = sqlite3.connect(file_path, check_same_thread=False)
db_con.row_factory = sqlite3.Row


def query_table(table, page, page_size, order_by=None, order=None):
    with db_con:
        cursor = db_con.cursor()
        if order is not None and order_by is not None:
            cursor.execute('SELECT * FROM %s ORDER BY %s %s LIMIT ?, ?' % (table, order_by, order),
                           (page_size * page, page_size))
        else:
            cursor.execute('SELECT * FROM %s LIMIT ?, ?' % table, (page_size * page, page_size))
        rows = cursor.fetchall()
        return rows


def insert_table(table, number_of_rows, rows):
    fields = []
    for i in range(1, number_of_rows):
        fields.append('?')
    sql = 'INSERT INTO %s VALUES(%s)' % (table, ','.join(fields))
    with db_con:
        cursor = db_con.cursor()
        cursor.executemany(sql, rows)


def insert_dict_into_table(table, array_of_dict):
    rows = []
    number_of_columns = 0
    column_names = []
    for obj in array_of_dict:
        tmp = []
        tmp.extend(obj.values())
        rows.append(tmp)
        number_of_columns = len(obj.keys())
        column_names = obj.keys()
    columns_placeholders = []
    for i in range(0, number_of_columns):
        columns_placeholders.append('?')
    placeholders = ','.join(columns_placeholders)
    column_names = ','.join(column_names)
    sql = 'INSERT INTO %s (%s) VALUES(%s)' % (table, column_names, placeholders)
    with db_con:
        cursor = db_con.cursor()
        cursor.executemany(sql, rows)


def update_dict_in_table(table, dict_to_update, id_val):
    column_names = dict_to_update.keys()
    values = dict_to_update.values()
    number_of_columns = len(column_names)
    columns_placeholders = []
    for i in range(0, number_of_columns):
        columns_placeholders.append('%s=?' % column_names[i])
    placeholders = ','.join(columns_placeholders)
    sql = 'UPDATE %s SET %s WHERE %s.id = %d' % (table, placeholders, table, id_val)
    with db_con:
        cursor = db_con.cursor()
        cursor.execute(sql, values)


def count_table(table):
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('SELECT COUNT(*) as records_cnt FROM %s' % table)
        result = cursor.fetchone()
        return result['records_cnt']


def insert_records(rows):
    with db_con:
        cursor = db_con.cursor()
        cursor.executemany('INSERT INTO records VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', rows)


def insert_prices(rows):
    with db_con:
        cursor = db_con.cursor()
        cursor.executemany('INSERT INTO prices VALUES(?, ?, ?, ?, ?)', rows)


def insert_clients(rows):
    with db_con:
        cursor = db_con.cursor()
        cursor.executemany("""INSERT INTO clients (sender_name, sender_cellphone, manager, client_name, client_rate,
proxy_rate, description, account_name) VALUES(?, ?, ?, ?, ?, ?, ?, ?)""", rows)


def clear_prices():
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('DELETE FROM prices')


def clear_clients():
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('DELETE FROM clients')


def clear_records():
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('DELETE FROM records')


def delete_client(client_id):
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('DELETE FROM clients WHERE clients.id = ?', (client_id,))


def clear_special_rates():
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('DELETE FROM special_rate')


def delete_special_rate(rate_id):
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('DELETE FROM special_rate WHERE special_rate.id = ?', (rate_id,))


def query_table_with_pagination(table, page, page_size, order_by=None, order=None):
    rows = query_table(table, page, page_size, order_by, order)
    total_entries = count_table(table)
    total_pages = int(ceil(float(total_entries) / float(page_size)))
    entries = []
    for row in rows:
        obj = {}
        for key in row.keys():
            obj[key] = row[key]
        entries.append(obj)
    result = [({
        'page': page + 1,
        'per_page': page_size,
        'total_entries': total_entries,
        'total_pages': total_pages
    }), entries]
    return result


def calculate_statistics_with_details(where):
    rows = query_statistics(where)
    details = []
    sums = {}
    for row in rows:
        client_name = row['client_name']
        client_rate = row['s_client_rate'] or row['client_rate'] or 1.0
        proxy_rate = row['s_proxy_rate'] or row['proxy_rate'] or client_rate or 1.0
        weight = row['weight'] or 0
        price = row['price']
        online_price = row['online_price']
        manager = row['manager']
        normal_price = weight * 0.001 * price + 8
        online_price = weight * 0.001 * online_price + 8
        proxy_price = normal_price * proxy_rate
        client_price = normal_price * client_rate
        price_difference = online_price - proxy_price
        client_refund = online_price - client_price
        profit = price_difference - client_refund
        detail = {
            'client_name': client_name,
            'manager': manager,
            'normal_price': normal_price,
            'online_price': online_price,
            'proxy_price': proxy_price,
            'client_price': client_price,
            'price_difference': price_difference,
            'client_refund': client_refund,
            'profit': profit,
            'country': row['country'],
            'serial_no': row['serial_no'],
            'items_count': row['items_count'],
            'weight': row['weight'],
            'recipient_name': row['recipient_name'],
            'recipient_address': row['recipient_address'],
            'recipient_cellphone': row['recipient_cellphone'],
            'recipient_tel': row['recipient_tel'],
            'sender_name': row['sender_name'],
            'sender_address': row['sender_address'],
            'sender_cellphone': row['sender_cellphone'],
            'sender_tel': row['sender_tel'],
            'batch_no': row['batch_no'],
            'record_date': row['record_date']
        }
        details.append(detail)
        if client_name not in sums:
            sums[client_name] = {
                'client_name': client_name,
                'normal_price_sum': 0,
                'online_price_sum': 0,
                'proxy_price_sum': 0,
                'client_price_sum': 0,
                'price_difference': 0,
                'client_refund': 0,
                'profit': 0,
                # This field is used to count serial_no
                'serial_no': 0
            }
        obj = sums[client_name]
        obj['manager'] = manager
        obj['normal_price_sum'] += normal_price
        obj['online_price_sum'] += online_price
        obj['proxy_price_sum'] += proxy_price
        obj['client_price_sum'] += client_price
        obj['price_difference'] += price_difference
        obj['client_refund'] += client_refund
        obj['profit'] += profit
        obj['serial_no'] += 1
    result = sums.values()
    return result, details


def calculate_statistics(where):
    return calculate_statistics_with_details(where)[0]


def count_statistics():
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('''SELECT COUNT(*) as records_cnt
FROM
(SELECT clients.client_name
FROM records
LEFT JOIN prices ON prices.country_name = records.country
LEFT JOIN clients ON records.sender_cellphone = clients.sender_cellphone
LEFT JOIN special_rate ON special_rate.client_name = clients.client_name
AND DATE(special_rate.start_date) <= DATE(records.record_date)
AND DATE(records.record_date) <= DATE(special_rate.end_date)
WHERE clients.client_name IS NOT NULL
GROUP BY clients.client_name)''')
        result = cursor.fetchone()
        return result['records_cnt']


def query_statistics(where):
    with db_con:
        cursor = db_con.cursor()
        cursor.execute('''
SELECT clients.client_name, clients.client_rate, clients.proxy_rate, clients.manager,
special_rate.client_rate as s_client_rate, special_rate.proxy_rate as s_proxy_rate,
records.*, prices.price, prices.online_price
FROM records
LEFT JOIN prices ON prices.country_name = records.country
LEFT JOIN clients ON records.sender_cellphone = clients.sender_cellphone
LEFT JOIN special_rate ON special_rate.client_name = clients.client_name
AND DATE(special_rate.start_date) <= DATE(records.record_date)
AND DATE(records.record_date) <= DATE(special_rate.end_date)
WHERE clients.client_name IS NOT NULL AND %s''' % where)
        rows = cursor.fetchall()
        return rows