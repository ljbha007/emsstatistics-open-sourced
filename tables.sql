BEGIN TRANSACTION;
CREATE TABLE "special_rate" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`client_name`	TEXT,
	`proxy_rate`	REAL,
	`client_rate`	REAL,
	`start_date`	TEXT,
	`end_date`	TEXT
);
CREATE TABLE "records" (
	`country`	TEXT,
	`serial_no`	TEXT,
	`items_count`	INTEGER,
	`weight`	INTEGER,
	`recipient_name`	TEXT,
	`recipient_address`	TEXT,
	`recipient_cellphone`	TEXT,
	`recipient_tel`	TEXT,
	`sender_name`	TEXT,
	`sender_address`	TEXT,
	`sender_cellphone`	TEXT,
	`sender_tel`	TEXT,
	`batch_no`	TEXT,
	`record_date`	TEXT
);
CREATE TABLE `prices` (
	`country_code`	TEXT UNIQUE,
	`country_name`	TEXT UNIQUE,
	`price_zone`	INTEGER,
	`price`	REAL,
	`online_price`	REAL
);
CREATE TABLE "clients" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`sender_name`	TEXT,
	`sender_cellphone`	TEXT UNIQUE,
	`manager`	TEXT,
	`client_name`	TEXT,
	`client_rate`	REAL DEFAULT 1.0,
	`proxy_rate`	REAL DEFAULT 1.0,
	`description`	TEXT,
	`account_name`	TEXT
);
COMMIT;
